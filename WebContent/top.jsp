<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>社内掲示板ホーム</title>

		<link rel="stylesheet" href="WebBBS.css">
		<link href="./css/style.css" rel="stylesheet" type="text/css">
		<link href="https://use.fontawesome.com/releases/v5.0.6/css/all.css" rel="stylesheet">
   	</head>
    <body>
        <div class="main-contents">
         <script type="text/javascript">
        	       <!--
					function MessageDelCheck() {
						if( window.confirm("この投稿を削除してよろしいですか？") ) {
							location.href = "/messagesdel";
						}
						else {
							alert("削除をやめました。");
							return false;
						}
					}
        	   	function CommentDelCheck() {
					if( window.confirm("このコメントを削除してよろしいですか？") ) {
						location.href = "/commentdel";
					}
					else {
						alert("削除をやめました。");
						return false;
					}
				}
        	   	-->
				</script>
			<div id="top">
				<div class="header">
					<p>社内掲示板ホーム画面</p>
				</div>


	            <div class="menu">
	          	 <a href="./" class="link" >ホーム</a>
	                <a href="newMessage" class="link">新規投稿</a>
	               	 <c:if test="${loginUser.branchId == 1 && loginUser.postId == 1 }">
		                <a href="management" class="link">ユーザー管理</a>
		             </c:if>

		        		<a href="logout"class="logout" >ログアウト</a>
	            </div>



	            <div class="search">
					<form action="./" method="get">
						<label for="name">カテゴリ検索</label> <input name="categorySearch" id="categorySearch" value="${categorySearch }"/> <br />
						<label for="name">日付検索</label> <input type="date" name="startDate" id="startDate" value="${startDate }" />
						<label for="name">～</label> <input type="date" name="stopDate" id="stopDate" value="${endDate}"/>
						<input type="submit" value="検索" class="subbutton">
					</form>
				</div>
			</div>

			<div id="contents">
			<c:if test="${ not empty errorMessages }">
	                <div class="errorMessages">
	                    <ul>
	                        <c:forEach items="${errorMessages}" var="message">
	                            <li><c:out value="${message}" />
	                        </c:forEach>
	                    </ul>
	                </div>
	                <c:remove var="errorMessages" scope="session" />
	            </c:if>
	            <br>
			<c:forEach items="${messages}" var="message">
				<div class="messages">
					<div class="message">
						<div class="account-name">
						<h2><i class="far fa-clipboard"></i> 件名：<span class="subject"><c:out value="${message.subject}" /></span></h2>
						</div>
						<div class="text">
								<c:forEach var="text" items="${fn:split(message.text, '
								')}"> <c:out value="${text}" /><br>
								</c:forEach>
							<br><br><br>

							<span class="category">カテゴリ：<c:out value="${message.category}" /></span>
							　　　　　<span class="name">投稿者：<c:out value="${message.name}" /></span>
							  　　　　<span id="date"> <fmt:formatDate  value="${message.created_date}"
								pattern="yyyy/MM/dd HH:mm:ss" /></span>
						</div>
						<c:if test="${loginUser.id == message.user_id || (loginUser.branchId == 1 && loginUser.postId == 2) }" >
							<div class="messagedel">
								<form action="messagesdel" method="post">
								<input type="hidden" name="message_id" value="${message.id }">
								<input type="hidden" name="user_id" value="${message.user_id}">
								<input type="submit" value="投稿削除" class="subbutton" onClick="return MessageDelCheck()">
								</form>
							</div>
						</c:if>
					</div>
					<br>

					<div class="messageComments">
					<i class="far fa-comment fa-lg" ></i> コメント
						<c:forEach items="${comments}" var="comment">
							<c:if test="${message.id == comment.message_id}" >

								<div class="commenttext">

									<c:forEach var="text" items="${fn:split(comment.text, '
										')}"> <c:out value="${text}" /><br>
									</c:forEach>

									<br><br>
								<span class="name">from：<c:out value="${comment.name}" /></span>
								　　　<span id="date"><fmt:formatDate value="${comment.createdDate}" pattern="yyyy/MM/dd HH:mm:ss" /></span>
								</div>

								<c:if test="${loginUser.id == comment.user_id || (loginUser.branchId == 1 && loginUser.postId == 2)}" >
									<form action="commentsdel" method="post">
										<input type="hidden" name="comment_id" value="${comment.id }">
										<input type="hidden" name="user_id" value="${comment.user_id}">
										<input type="submit" value="コメント削除" class="subbutton" onClick="return CommentDelCheck()">
									</form>
								</c:if>
								<br>
							</c:if>
						</c:forEach>
					</div>
<br>
            		<div class="comment">
						<form action="comment" method="post">
							コメント(500文字以内)<br />
							<textarea name="text" cols="100" rows="5"  ><c:if test="${message.id == democomment.message_id }">${democomment.text}<c:remove var="democomment" scope="session" /></c:if></textarea>
								<br /> <input type="hidden" name="message_id" value="${message.id}">
								 <input type="submit" class="subbutton" value="コメント">
						</form>
					</div>
				</div>
				<br><br>
			</c:forEach>

			</div>

	    </div>


        	<div class="copyright"> Copyright(c)R.Tanaka</div>
    </body>
</html>
