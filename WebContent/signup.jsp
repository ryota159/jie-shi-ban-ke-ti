<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>ユーザー登録</title>
<link rel="stylesheet" href="WebBBS.css">
 <link href="./css/style.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        <div class="main-contents">
	        <div id="top">
				<div class="header">
					<p>ユーザー登録画面</p>
				</div>
         <a href="management" class="link">ユーザー管理画面へ</a>



        </div>

		<br>
		<div style="margin-top:130px;">
 			<c:if test="${ not empty errorMessages }">
                <div class="errorMessages" >
                    <ul  style="display: inline-block; text-align: left;">
                        <c:forEach items="${errorMessages}" var="message">
                            <li><c:out value="${message}" />
                        </c:forEach>
                    </ul>
             </div>
                <c:remove var="errorMessages" scope="session" />
            </c:if>
        </div>
			<div class="userset">
			<form action="signup" method="post">
                <br /> <label for="loginId">ログインID<span>(半角英数字6-20文字)</span></label><br>
                <input name="loginId" id="loginId"   size="30" value="${userinfo.loginId }"/> <br />


				<label for="password">パスワード<span>(半角文字6-20文字)</span></label><br>
				<input name="password" type="password" size="30" id="password" /> <br />
				 <label for="passwordConfirm">パスワード<span>(確認用)</span></label><br>
				 <input type="password" name="passwordConfirm" size="30" id="passwordConfirm" > <br />

                <label for="name">ユーザー名<span>(10文字以下)</span></label><br>
                <input name="name" id="name" size="30"  value="${userinfo.name }"/> <br />
                <label for="branchId">支店名</label> <br />
                 <select name="branchId" size="1"  >
                 <c:forEach items="${branches }" var="branch"><option value="${branch.id}" ${(branch.id == userinfo.branchId) ? 'selected' : ''}><c:out value="${branch.name}"/>
                 </option></c:forEach></select>
                  <br/>
				 <label for="postId">部署・役職名</label> <br />
				 <select name="postId" size="1"  >
				  <c:forEach items="${posts }" var="post"><option value="${post.id}" ${(post.id == userinfo.postId) ? 'selected' : "" }><c:out value="${post.name}"/>
                 </option></c:forEach></select>
                 <br/>
                <br /> <input type="submit" class="subbutton" value="登録" /> <br />
              </form>
           </div>
			<br>
            <div class="copyright">Copyright(c)R.Tanaka</div>
        </div>
    </body>
</html>