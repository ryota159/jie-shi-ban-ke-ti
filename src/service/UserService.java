package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import com.mysql.jdbc.StringUtils;

import beans.UserBeans;
import beans.UserManagementBean;
import dao.UserDao;
import dao.UserManagement;
import utils.CipherUtil;

public class UserService {

    public void register(UserBeans user) {

        Connection connection = null;
        try {
            connection = getConnection();

            String encPassword = CipherUtil.encrypt(user.getPassword());
            user.setPassword(encPassword);

            UserDao userDao = new UserDao();
            userDao.insert(connection, user);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
    private static final int LIMIT_NUM = 1000;

    public List<UserManagementBean> getUserManagement() {

        Connection connection = null;
        try {
            connection = getConnection();

            UserManagement messageDao = new UserManagement();
            List<UserManagementBean> ret = messageDao.getUserManagement(connection, LIMIT_NUM);

            commit(connection);

            return ret;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    public UserBeans getUserEdit(int user_id) {

        Connection connection = null;
        try {
            connection = getConnection();

            UserDao userDao = new UserDao();
            UserBeans ret = userDao.getUserEdit(connection,user_id );

            commit(connection);

            return ret;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    public void userEdit(UserBeans user) {



        Connection connection = null;
        try {
            connection = getConnection();

            if(StringUtils.isNullOrEmpty(user.getPassword()) != true){
            String encPassword = CipherUtil.encrypt(user.getPassword());
            user.setPassword(encPassword);
            }


            UserDao userDao = new UserDao();
            userDao.edit(connection, user);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    public void userIsStopped(int id, int isStopped) {

        Connection connection = null;
        try {
            connection = getConnection();

            UserDao userDao = new UserDao();
            userDao.isStopped(connection, id,isStopped);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

}