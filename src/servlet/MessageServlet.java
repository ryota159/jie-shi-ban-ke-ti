package servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.mysql.jdbc.StringUtils;

import beans.MessageBeans;
import beans.UserBeans;
import service.MessageService;


@WebServlet("/newMessage")
public class MessageServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	 protected void doGet(HttpServletRequest request,
			 HttpServletResponse response) throws IOException, ServletException {


		request.getRequestDispatcher("/newMessage.jsp").forward(request, response);
	}




	@Override
	 protected void doPost(HttpServletRequest request,
			 HttpServletResponse response) throws IOException, ServletException {

        HttpSession session = request.getSession();

        UserBeans user = (UserBeans) session.getAttribute("loginUser");

        List<String> messages = new ArrayList<String>();

        if (isValid(request, messages) == true) {


        MessageBeans message = new MessageBeans();
        message.setSubject(request.getParameter("subject"));
        message.setText(request.getParameter("text"));
        message.setCategory(request.getParameter("category"));
        message.setUser_id(user.getId());

        new MessageService().register(message);

        response.sendRedirect("./");
        } else {
        	MessageBeans message = new MessageBeans();
        	 message.setSubject(request.getParameter("subject"));
             message.setText(request.getParameter("text"));
             message.setCategory(request.getParameter("category"));

            session.setAttribute("errorMessages", messages);
            request.setAttribute("message", message);

            request.getRequestDispatcher("/newMessage.jsp").forward(request, response);
        }
	}
	 private boolean isValid(HttpServletRequest request, List<String> messages ) {
	        String subject = request.getParameter("subject");
	        String text = request.getParameter("text");
	        String category = request.getParameter("category");





	        if (StringUtils.isNullOrEmpty(subject) == true) {
	            messages.add("件名を入力してください");
	        }
	        if (subject.matches("^[ 　]+$")) {
	            messages.add("件名を入力してください");
	        }
	        if(subject.length()>30){
	        	messages.add("件名は30文字以内です");
	        }


	        if (StringUtils.isNullOrEmpty(category) == true) {
	            messages.add("カテゴリ名を入力してください");
	        }
	        if (category.matches("^[ 　]+$")) {
	            messages.add("カテゴリ名を入力してください");
	        }
	        if(category.length()>10){
	        	messages.add("カテゴリ名は10文字以内です");
	        }

	        if (StringUtils.isNullOrEmpty(text) == true) {
	            messages.add("本文を入力してください");
	        }
	        if (text.matches("^[ 　]+$")) {
	            messages.add("本文を入力してください");
	        }
	        if(text.length()>1000){
	        	messages.add("本文は1000文字以内です");
	        }

	        if (messages.size() == 0) {
	            return true;
	        } else {
	            return false;
	        }
	    }
}
