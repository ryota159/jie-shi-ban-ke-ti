package servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.UserBeans;
import beans.UserManagementBean;
import service.UserService;

@WebServlet(urlPatterns = { "/signup" })
public class SignUpServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        request.getRequestDispatcher("signup.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        List<String> messages = new ArrayList<String>();

        HttpSession session = request.getSession();
        if (isValid(request, messages) == true) {

            UserBeans user = new UserBeans();
            user.setLoginId(request.getParameter("loginId"));
            user.setPassword(request.getParameter("password"));
            user.setName(request.getParameter("name"));
            user.setBranchId(Integer.parseInt(request.getParameter("branchId")));
            user.setPostId(Integer.parseInt(request.getParameter("postId")));

            new UserService().register(user);

            response.sendRedirect("./");
        } else {
            UserBeans userinfo = new UserBeans();

            userinfo.setLoginId(request.getParameter("loginId"));
            userinfo.setPassword(request.getParameter("password"));
            userinfo.setName(request.getParameter("name"));
            userinfo.setBranchId(Integer.parseInt(request.getParameter("branchId")));
            userinfo.setPostId(Integer.parseInt(request.getParameter("postId")));

            request.setAttribute("userinfo", userinfo);
            session.setAttribute("errorMessages", messages);
            request.getRequestDispatcher("signup.jsp").forward(request, response);
        }
    }

    private boolean isValid(HttpServletRequest request, List<String> messages) {
        String loginId = request.getParameter("loginId");
        String password = request.getParameter("password");
        String passwordConfirm = request.getParameter("passwordConfirm");
        String name = request.getParameter("name");
        int branch_id = Integer.parseInt(request.getParameter("branchId"));
        int post_id = Integer.parseInt(request.getParameter("postId"));


        if (StringUtils.isEmpty(loginId) == true) {
            messages.add("ログインIDを入力してください");
        }else if(!(loginId.matches("^[a-zA-Z0-9]{6,20}$"))){
        	messages.add("ログインIDは半角英数字で6-20文字の範囲で入力して下さい");
        }

        List<UserManagementBean> users = new UserService().getUserManagement();
        boolean userLog = true;
        for(UserManagementBean user : users){
        	if(user.getLoginId().matches(loginId)){
        		userLog = false;
        	}
        }
        if(userLog == false){
        	messages.add("すでに利用されているログインIDです");
        }

        if(name.length()>10){
        	messages.add("ユーザー名は10文字以内です");
        }
        // TODO アカウントが既に利用されていないか、メールアドレスが既に登録されていないかなどの確認も必要

        if (StringUtils.isEmpty(password) == true  || password.matches("^[ 　]+$")) {
            messages.add("パスワードを入力してください");
        }else if (!(password.matches("^[ -~｡-ﾟ]{6,20}$"))) {
            messages.add("パスワードは半角文字で6-20文字の範囲で入力して下さい");
        }
        if (!password.matches(passwordConfirm)){
        	messages.add("パスワードが一致していません");
        }

        if (StringUtils.isEmpty(name) == true || name.matches("^[ 　]+$")) {
            messages.add("ユーザー名を入力してください");
        }

        boolean userPost = true;
        if(branch_id != 1){
        for(UserManagementBean user : users){
        	if(user.getBranchId() == branch_id && user.getPostId() == 3 && user.getPostId() == post_id){
        		userPost = false;
        	}
        }
        if(userPost == false){
        	messages.add("すでに店長が登録されています");
        }
        }

        if(branch_id == 1 ){
        	if(!(post_id == 1 )){
        		if(!(post_id == 2)){
        			messages.add("支店名と部署・役職名の組み合わせが無効です");
        		}
        	}
        }else  if(!(post_id == 3 )){
    		if(!(post_id == 4)){
    			messages.add("支店名と部署・役職名の組み合わせが無効です");
    		}
        }
        if (messages.size() == 0) {
            return true;
        } else {
            return false;
        }
    }

}
