package servlet;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.mysql.jdbc.StringUtils;

import beans.MessageComment;
import beans.UserMessage;
import service.CommentService;
import service.MessageService;


@WebServlet(urlPatterns = { "/index.jsp" })
public class TopServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

//    	if(request.getSession().getAttribute("loginUser")==null) {
//            response.sendRedirect("login");
//            return;
//    	}

//        UserBeans user = (UserBeans) request.getSession().getAttribute("loginUser");
//        boolean isShowMessageForm;
//        if (user != null) {
//            isShowMessageForm = true;
//        } else {
//            isShowMessageForm = false;
//        }

        String startDate = request.getParameter("startDate");
        String stopDate = request.getParameter("stopDate");
        String categorySearch = request.getParameter("categorySearch");


        if(StringUtils.isNullOrEmpty(stopDate)){
        	Date dt = new Date();
        	SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
        	stopDate = sdf.format(dt);
        }
//        if(categorySearch == null ){
//        	categorySearch = "*";
//        }

        List<UserMessage> messages = new MessageService().getMessage(startDate, stopDate, categorySearch);
        List<MessageComment> comments = new CommentService().getComment();

        HttpSession session = request.getSession();


        request.setAttribute("democomment", request.getAttribute("demo"));
        request.setAttribute("messages", messages);
        request.setAttribute("comments", comments);
        request.setAttribute("startDate", startDate);
        request.setAttribute("endDate", stopDate);
        request.setAttribute("categorySearch", categorySearch);


//        request.setAttribute("isShowMessageForm", isShowMessageForm);


        request.getRequestDispatcher("/top.jsp").forward(request, response);

    }

}
