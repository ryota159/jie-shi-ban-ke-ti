package servlet;


import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.BranchBean;
import beans.PostBean;
import beans.UserBeans;
import service.BranchService;
import service.LoginService;
import service.PostService;

@WebServlet(urlPatterns = { "/login" })
public class LoginServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        request.getRequestDispatcher("login.jsp").forward(request, response);;
    }

    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        String loginId = request.getParameter("loginId");
        String password = request.getParameter("password");



        LoginService loginService = new LoginService();
        UserBeans user = loginService.login(loginId, password);

        HttpSession session = request.getSession();
        if((user==null)||(user.getIsStopped()==0)){
            List<String> messages = new ArrayList<String>();
            messages.add("ログインに失敗しました");
            session.setAttribute("errorMessages", messages);
            request.setAttribute("loginId", loginId);
            request.getRequestDispatcher("/login.jsp").forward(request, response);
            return;

        }

        if (user != null) {

            List<BranchBean> branch =  new BranchService().getBranchList();
            Collections.sort(branch, new Comparator<BranchBean>(){
	            @Override
	            public int compare(BranchBean a, BranchBean b){
	              return a.getId() - b.getId();
	            }
        		});

            List<PostBean> post =  new PostService().getPostList();
            Collections.sort(post, new Comparator<PostBean>(){
	            @Override
	            public int compare(PostBean a, PostBean b){
	              return a.getId() - b.getId();
	            }
        		});

            session.setAttribute("branches",branch);
            session.setAttribute("posts",post);

            session.setAttribute("loginUser", user);
            response.sendRedirect("./");

        }
    }

}
