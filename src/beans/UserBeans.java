package beans;


import java.io.Serializable;
import java.util.Date;

public class UserBeans implements Serializable {
    private static final long serialVersionUID = 1L;

    private int id;
    private String loginId;
    private String password;
    private String name;
    private int branchId;
    private int postId;
    private int isStopped;
    private Date createdDate;
    private Date updatedDate;

	public int getId() {
	    return id;
	}

	public void setId(int id) {
	    this.id = id;
	}

	public String getLoginId() {
	    return loginId;
	}

	public void setLoginId(String loginId) {
	    this.loginId = loginId;
	}

	public String getPassword() {
	    return password;
	}

	public void setPassword(String password) {
	    this.password = password;
	}

	public String getName() {
	    return name;
	}

	public void setName(String name) {
	    this.name = name;
	}

	public int getBranchId() {
	    return branchId;
	}

	public void setBranchId(int branchId) {
	    this.branchId = branchId;
	}

	public int getPostId() {
	    return postId;
	}

	public void setPostId(int postId) {
	    this.postId = postId;
	}

	public int getIsStopped() {
	    return isStopped;
	}

	public void setIsStopped(int isStopped) {
	    this.isStopped = isStopped;
	}

	public Date getCreatedDate() {
	    return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
	    this.createdDate = createdDate;
	}

	public Date getUpdatedDate() {
	    return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
	    this.updatedDate = updatedDate;
	}

}