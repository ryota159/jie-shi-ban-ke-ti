package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import com.mysql.jdbc.StringUtils;

import beans.UserBeans;
import exception.SQLRuntimeException;

public class UserDao {

    public void insert(Connection connection, UserBeans user) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO users ( ");
            sql.append("login_id");
            sql.append(", password");
            sql.append(", name");
            sql.append(", branch_id");
            sql.append(", post_id");
            sql.append(", is_stopped");
            sql.append(", created_date");
            sql.append(", updated_date");
            sql.append(") VALUES (");
            sql.append("?"); // login_id
            sql.append(", ?"); // password
            sql.append(", ?"); // name
            sql.append(", ?"); // branch_id
            sql.append(", ?"); // post_id
            sql.append(", 1"); // is_stopped
            sql.append(", CURRENT_TIMESTAMP"); // created_date
            sql.append(", CURRENT_TIMESTAMP"); // updated_date
            sql.append(")");

            ps = connection.prepareStatement(sql.toString());

            ps.setString(1, user.getLoginId());
            ps.setString(2, user.getPassword());
            ps.setString(3, user.getName());
            ps.setInt(4, user.getBranchId());
            ps.setInt(5, user.getPostId());


            ps.executeUpdate();
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }


    public void edit(Connection connection, UserBeans user) {


        if(StringUtils.isNullOrEmpty(user.getPassword()) == true){
    		  PreparedStatement ps = null;
    	        try {
    	            StringBuilder sql = new StringBuilder();
    	            sql.append("UPDATE users set");
    	            sql.append(" login_id = ?");
    	            sql.append(", name = ?");
    	            sql.append(", branch_id = ?");
    	            sql.append(", post_id = ?");
    	            sql.append(", updated_date = CURRENT_TIMESTAMP");
    	            sql.append(" where ");
    	            sql.append("id = ? ");


    	            ps = connection.prepareStatement(sql.toString());

    	            ps.setString(1, user.getLoginId());
    	            ps.setString(2, user.getName());
    	            ps.setInt(3, user.getBranchId());
    	            ps.setInt(4, user.getPostId());
    	            ps.setInt(5, user.getId());



    	            ps.executeUpdate();
    	        } catch (SQLException e) {
    	            throw new SQLRuntimeException(e);
    	        } finally {
    	            close(ps);
    	        }

    	  }else{
        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("UPDATE users set");
            sql.append(" login_id = ?");
            sql.append(", password = ?");
            sql.append(", name = ?");
            sql.append(", branch_id = ?");
            sql.append(", post_id = ?");
            sql.append(", updated_date = CURRENT_TIMESTAMP");
            sql.append(" where ");
            sql.append("id = ? ");


            ps = connection.prepareStatement(sql.toString());

            ps.setString(1, user.getLoginId());
            ps.setString(2, user.getPassword());
            ps.setString(3, user.getName());
            ps.setInt(4, user.getBranchId());
            ps.setInt(5, user.getPostId());
            ps.setInt(6, user.getId());



            ps.executeUpdate();
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    	  }
    }


    public void isStopped(Connection connection, int id, int isStopped) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("UPDATE users set");
            sql.append(" is_stopped = ?");
            sql.append(", updated_date = CURRENT_TIMESTAMP");
            sql.append(" where ");
            sql.append("id = ? ");


            ps = connection.prepareStatement(sql.toString());

            ps.setInt(1, isStopped);
            ps.setInt(2, id);

            ps.executeUpdate();
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

public UserBeans getUser(Connection connection, String loginId,
        String password) {

    PreparedStatement ps = null;
    try {
        String sql = "SELECT * FROM users WHERE login_id = ? AND password = ?";

        ps = connection.prepareStatement(sql);
        ps.setString(1, loginId);
        ps.setString(2, password);


        ResultSet rs = ps.executeQuery();
        List<UserBeans> userList = toUserList(rs);

        if (userList.isEmpty() == true) {
            return null;
        } else if (2 <= userList.size()) {
            throw new IllegalStateException("2 <= userList.size()");
        } else {
            return userList.get(0);
        }
    } catch (SQLException e) {
        throw new SQLRuntimeException(e);
    } finally {
        close(ps);
    }
}

public UserBeans getUserEdit(Connection connection, int user_id) {

    PreparedStatement ps = null;
    try {
        String sql = "SELECT * FROM users WHERE id = ? ";

        ps = connection.prepareStatement(sql);
        ps.setInt(1, user_id);



        ResultSet rs = ps.executeQuery();
        List<UserBeans> userList = toUserList(rs);

        if (userList.isEmpty() == true) {
            return null;
        } else if (2 <= userList.size()) {
            throw new IllegalStateException("2 <= userList.size()");
        } else {
            return userList.get(0);
        }
    } catch (SQLException e) {
        throw new SQLRuntimeException(e);
    } finally {
        close(ps);
    }
}

private List<UserBeans> toUserList(ResultSet rs) throws SQLException {

    List<UserBeans> ret = new ArrayList<UserBeans>();
    try {
        while (rs.next()) {
            int id = rs.getInt("id");
            String loginId = rs.getString("login_id");
            String password = rs.getString("password");
            String name = rs.getString("name");
            int branchId = rs.getInt("branch_id");
            int postId = rs.getInt("post_id");
            int isStopped = rs.getInt("is_stopped");
            Timestamp createdDate = rs.getTimestamp("created_date");
            Timestamp updatedDate = rs.getTimestamp("updated_date");

            UserBeans user = new UserBeans();
            user.setId(id);
            user.setLoginId(loginId);
            user.setPassword(password);
            user.setName(name);
            user.setBranchId(branchId);
            user.setPostId(postId);
            user.setIsStopped(isStopped);
            user.setCreatedDate(createdDate);
            user.setUpdatedDate(updatedDate);

            ret.add(user);
        }
        return ret;
    } finally {
        close(rs);
    }
}
}
